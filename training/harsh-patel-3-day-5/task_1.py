'''
Given a xml file(sample.xml) print out all the files and folders hierarchically.
Take the filename as an argument. Use OOPS concepts.
Python script should contain class and all the required
definitions should be defined within that class.
'''

# (\\10.0.1.22\CrestData\User Data\hardik Shingala\Python Training\sample.xml)

import xml.etree.ElementTree as ET
import json

class XMLHandler:
    @staticmethod
    def get_root(file_name="demo_folders/demo.xml"):
        '''
        Main function
        '''
        node = ET.parse(file_name)
        root = node.getroot()
        return root

    @classmethod
    def get_data(cls, root, position=1):
        '''
        Method to get data
        '''
        for child in root:
            if child.tag == 'folder':
                print("\t "*position + "| -  {0}".format(child.attrib['name']))
                position += 1
                cls.get_data(child, position)
                position -= 1
            else:
                print("\t "*position + "| -  {0}".format(child.attrib['name']))

if __name__ == "__main__":
    main_root = XMLHandler.get_root()
    print("\t{0}".format(main_root.attrib['name']))
    XMLHandler().get_data(main_root)
