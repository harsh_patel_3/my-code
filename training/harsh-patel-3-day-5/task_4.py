'''
Create a json file in following format.
	{
		""folder_name_1"": [filename_1, filename_2, ....],
		""folder_name_2"": [filename_1, filename_2, ....]
		.
		.
		.
	}
   Fetch folder names from the json file and if that folder is present
   in a predefined location using pythonic way, check if all the files
   mentioned in the list is present or not.
   Print all the files which are not present in the folder.
'''

import task_1
import xml.etree.ElementTree as ET
import json
import os

class JSONHandler:
	@staticmethod
	def get_folder_data(file_data, path="test_folders"):
		folder_not_present = []
		folder_at_path = os.listdir(path)
		print("The file(s) not present at the specified path is/are:")
		for folder_in_file in file_data:
			if folder_in_file in folder_at_path:
				files_at_path = os.listdir(path + "/{0}".format(str(folder_in_file)))
				for file_in_folder_in_file in file_data[folder_in_file]:
					if file_in_folder_in_file not in files_at_path:
						print("\t{0}/{1}/{2}".format(path, folder_in_file, file_in_folder_in_file))
			else:
				folder_not_present.append("\t{0}\n".format(folder_in_file))
		print("The folder(s) not present at specified location is/are:")
		print(*folder_not_present)

	@staticmethod
	def read_file():
		read_json = open('demo_folders/data.json')
		file_data = json.load(read_json)
		read_json.close()
		return file_data

class XMLHandler:
	@staticmethod
	def get_data_from_XML(root, json_list_files={}):
		empty_list = False
		for root_child in root:
			if root_child.tag == 'folder':
				XMLHandler.get_data_from_XML(root_child, json_list_files)
			elif root_child.tag == 'file' and empty_list is False:
				json_list_files[root.attrib['name']] = [root_child.attrib['name']]
				empty_list = True
			elif root_child.tag == 'file' and empty_list is True:
				json_list_files[root.attrib['name']].append(root_child.attrib['name'])
		return json_list_files

	@staticmethod
	def main():
		main_root = task_1.XMLHandler.get_root()
		json_list_files = XMLHandler.get_data_from_XML(main_root)

		json_data = open('demo_folders/data.json', 'w')
		json.dump(json_list_files, json_data, indent=4)
		json_data.close()

if __name__ == "__main__":
	XMLHandler.main()
	file_data = JSONHandler.read_file()
	JSONHandler.get_folder_data(file_data)
