'''
Write a function that takes json file(demo.json) as an input and
return value for any of the requested key.
    (The key may be at any level)
    =============
    sample input
    =============

    md5Hex

    =============
    desired ouput
    =============

    377d484478843e5e2d8b7eb935cbf598
'''
# (\\10.0.1.22\CrestData\UserData\Jay Joshi\Python handson\demo.json)

import json
import sys

class JSONHandler:
    '''
    Class to handle JSON file
    '''
    @staticmethod
    def get_data(json_data, input_data, count=0):
        '''
        Get input_data from input jsn_data
        '''
        for key in json_data:
            if isinstance(json_data[key], dict):
                count = JSONHandler.get_data(json_data[key], input_data, count)
                return count

            if isinstance(json_data[key], list):
                for list_data in json_data[key]:
                    count = JSONHandler.get_data(list_data, input_data, count)
                    return count

            if key == input_data:
                print("\nWord found!!")
                print("{0}\n".format(json_data[key]))
                count += 1
                return count

    @staticmethod
    def reading_input(json_data):
        '''
        Reads input and pass to get_data()
        '''
        input_data = input("Enter the word to search: ")
        count = JSONHandler.get_data(json_data, input_data)

        if count is None:
            print("\nNo data found!!\n")

    @staticmethod
    def initial_process():
        '''
        Initial process being called once
        '''
        file_name = "demo_folders/demo.json"
        json_file = open(file_name, 'r')
        json_data = json.load(json_file)
        JSONHandler.reading_input(json_data)

        while True:
            print("Do you want to continue?")
            print("Print y|Y to continue.")
            print("Print n|N to exit.")
            option = input("Enter your option: ")
            print()

            if option in ('y', 'Y'):
                JSONHandler.reading_input(json_data)

            if option in ('n', 'N'):
                sys.exit("Exiting...")

            else:
                print("Please enter a valid option!!")

        json_file.close()

if __name__ == "__main__":
    JSONHandler.initial_process()
