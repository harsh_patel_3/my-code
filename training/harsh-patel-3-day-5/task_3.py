'''
Create a Python script that will search a predetermined directory and list the items within,
separating the files and directories into two lists that will be printed to the screen.
'''
import os

class Show_Data:
    @staticmethod
    def show_directories(dir_list, path):
        '''
        Method to show directries
        '''
        dir_heading = "{0}{1}{2}".format("\n", len(dir_list), " directories of " +
                                        os.path.abspath(path))
        dir_heading_len = len(dir_heading)
        print(dir_heading)
        print("."*dir_heading_len)

        for index, dir_name in enumerate(sorted(dir_list)):
            print(str(index+1) + ".", dir_name)

    @staticmethod
    def show_files(file_list, path):
        '''
        Method to show files
        '''
        file_heading = "{0}{1}{2}".format("\n", len(file_list), " files of " +
                                        os.path.abspath(path))
        file_heading_len = len(file_heading)
        print(file_heading)
        print("."*file_heading_len)
        for index, file_name in enumerate(sorted(file_list)):
            print(str(index+1) + ".", file_name)

    @staticmethod
    def show_contents(path="."):
        '''
        Method to get contents
        '''
        file_list = []
        dir_list = list()
        try:
            for content in os.listdir(path):
                if os.path.isfile(os.path.join(path, content)):
                    file_list.append(content)
                else:
                    dir_list.append(content)
        except FileNotFoundError:
            print("\nError, once check the path")
            return
        Show_Data.show_files(file_list, path)
        Show_Data.show_directories(dir_list, path)

class Input_Data:
    @staticmethod
    def input_data():
        '''
        Method to input data
        '''
        print("Please enter the path to search for.")
        path = input("Leave blank to search the current directory: ")

        if not path:
            print("Printing content of current dir")
            Show_Data.show_contents()

        else:
            print("Printing content of " + path)
            Show_Data.show_contents(path)

    @staticmethod
    def main():
        '''
        Main function
        '''
        Input_Data.input_data()

        while True:
            print()
            print("Do you want to continue?")
            print("Press y|Y to continue.")
            print("Press n|N to exit.")
            option = input("Enter your option: ")

            if option in ('y', 'Y'):
                Input_Data.input_data()

            elif option in ('n', 'N'):
                break

            else:
                print("Please enter a valid option!!")

if __name__ == "__main__":
    Input_Data.main()
