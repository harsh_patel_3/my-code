"""
Use Complex classes created in 'Classes' module and
serialize-deserialize objects of them

"""
import pickle
import json
from serialize import SerDe
class Person:
    """ Simple person class """

    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender


class Employee(Person):
    """ Employee class inherited from Person """

    def __init__(self, name, age, gender, eid, hobbies):
        super().__init__(name, age, gender)
        self.eid = eid
        self.hobbies = hobbies

    def show(self):
        print("Name:" + self.name + "  Age:" + self.age +
              "  Gender:" + self.gender + " Employee id:" + self.eid)
        print("Hobbies:", self.hobbies)

class Main:
    """ Main class implementation """

    def main(self):
        """ taking input of complex class and serializing it """
        ename, age, gender, eid = input(
            "Enter Employee name, age, gender, eid:\n").split()
        hobbies = list(input("Enter hobbies:").split())
        obj1 = Employee(ename, age, gender, eid, hobbies)
        print("Before Serialization...")
        print(obj1.show()) 
        se_obj = SerDe()
        
        # Pickle SerDe
        pickle1 = se_obj.pickle_serialize(obj1)
        print(pickle1)
        pickle2 = se_obj.pickle_deserialize(pickle1)
        print(pickle2.show())
        
        # Json SerDe
        json1 = se_obj.json_serialize(obj1)
        print(json1)
        json2 = se_obj.json_deserialize(json1)
        json2 = Employee(**json2)
        print(json2.show())

if __name__ == "__main__":
    Main().main()
