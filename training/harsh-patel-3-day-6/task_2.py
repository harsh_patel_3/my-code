"""
Create a simple class(e.g. Employee) with some attributes(e.g. name, age, gender) and
serialize-deserialize objects using Pickle.(Take values from user as inputs at the first
time and to update them at the second time after deserializing and then serialize them

"""
import pickle
from serialize import SerDe
class Employee:
    """ Simple employee class """
    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender
    def __str__(self):
        return "Name:" + self.name + "  Age:" + self.age + "  Gender:" + self.gender

class Main:
    """ implemented main class """
    def main(self):
        """ taking input from user and serialize and deserialize it """
        ename, age, gender = input("Enter Employee name, age and gender:\n").split()
        obj1 = Employee(ename, age, gender)
        print("Before Serialization...")
        print(obj1)
        se_obj = SerDe()
        # Pickle SerDe
        pickle1 = se_obj.pickle_serialize(obj1)
        print(pickle1)
        # Updating details
        ename, age, gender = input("Enter Updated Employee name, age and gender:\n").split()
        pickle2 = se_obj.pickle_deserialize(pickle1)
        print(pickle2)
        pickle2.name = ename
        pickle2.age = age
        pickle2.gender = gender
        print("After updating..")
        print(pickle2)
        pickle2 = se_obj.pickle_serialize(pickle2)
        print(pickle2)


if __name__ == "__main__":
    Main().main()

"""
Conclusions:
1)
JSON is a text serialization format,
while pickle is a binary serialization format;
2)
JSON is human-readable,
while pickle is not;
3)
To convert into json serialization we need dictionary object and 
after deserialization it returns dictionary that we need to convert into object,
while in pickle it returns object

"""
