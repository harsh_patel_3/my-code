""" Try Serialize file handler object """
import pickle
from serialize import SerDe
"""
Here we Cannot Serialize file Object as if you close file Descriptor there
is no way to obtain old object from unpickling without re-opening the file
"""
import json

with open('demo.json', 'r') as f:       
    # trying to serialize file handler object
    se_obj = SerDe()
    pickle1 = se_obj.pickle_serialize(f)
    print(pickle1)
    