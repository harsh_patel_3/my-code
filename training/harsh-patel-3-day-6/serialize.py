import json
import pickle
import sys

class SerDe:
    """ SerDe class to serialize and deserialize object  """
    def json_serialize(self, obj):
        try:
            print("After Json serialization:")
            return json.dumps(obj.__dict__, indent=4)
        except TypeError:
            print("Can't serialize...")
            sys.exit(1)

    def json_deserialize(self, obj):
        try:
            print("After Json deserialization:")
            return json.loads(obj)
        except TypeError:
            print("Can't Deserialize...")
            sys.exit(1)

    def pickle_serialize(self, obj):
        try:
            print("After Pickle serialization:")
            return pickle.dumps(obj)
        except TypeError:
            print("Can't serialize...")
            sys.exit(1)
    
    def pickle_deserialize(self, obj):
        try:
            print("After Pickle Deserialization:")
            return pickle.loads(obj)
        except TypeError:
            print("Can't Deserialize...")
            sys.exit(1)