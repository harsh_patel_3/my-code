"""
Create a simple class(e.g. Employee) with some attributes(e.g. name, age, gender) and
serialize-deserialize objects using JSON (Take values from user as inputs at the first
time and to update them at the second time after deserializing and then serialize them

"""
import json
from serialize import SerDe
class Employee:
    """ Simple employee class """

    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender

    def __str__(self):
        return "Name:" + self.name + "  Age:" + self.age + "  Gender:" + self.gender


class Main:
    def main(self):
        """ taking input from user and serialize and deserialize it """
        ename, age, gender = input("Enter Employee name, age and gender:\n").split()
        obj1 = Employee(ename, age, gender)
        print("Before Serialization...")
        print(obj1)
        se_obj = SerDe()
        # Json Seriaization
        json1 = se_obj.json_serialize(obj1)
        print(json1)
        
        ename, age, gender = input("Enter Updated Employee name, age and gender:\n").split()
        json2 = se_obj.json_deserialize(json1)
        json2 = Employee(**json2)
        print(json2)
        # updating values
        json2.name = ename
        json2.age = age
        json2.gender = gender
        print("After updating...")
        print(json2)
        # again serialize
        json2 = se_obj.json_serialize(json2)
        print(json2)


if __name__ == "__main__":
    Main().main()
