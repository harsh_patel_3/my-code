'''
Create a python class named Algorithms. It should have following sorting algorithms:
    Quick sort
    Bubble sort
    BFS (I/P Graph)
    DFS (I/P Graph)
    Merge Sort
Create a class Sorting which should sort the list provided as input parameter using
the algorithm which is also provided as input parameter.
Create a main class which inherits the Sorting class and calls sort method
with list and algorithm to use.
'''

import algorithms, sys

class Algorithms:
    def __init__(self, inp_list):
        self.inp_list = inp_list

    def quick_sort(self):
        return algorithms.class_quick_sort.quick_sort(self, self.inp_list)

    def bubble_sort(self):
        return algorithms.class_bubble_sort.bubble_sort(self, self.inp_list)

    def merge_sort(self):
        return algorithms.class_merge_sort.merge_sort(self, self.inp_list)

class Sorting:
    def sort(self, algo_name, inp_list):
        return Algorithms(inp_list).__getattribute__(algo_name)()

class Main(Sorting):
    def __init__(self, algo_name, inp_list):
        results = Sorting.sort(self, algo_name, inp_list)
        print("Sorted by {0}".format(algo_name))
        print(results)

def main():
    try:
        input_count = 3
        while input_count > 0:
            try:
                size_list = int(input("Enter the size of the list: "))
                input_count = 0

            except:
                if input_count > 0:
                    print("List size should be integer!!")
                    input_count -= 1
                    print("{0} trials left!!".format(input_count))

                else:
                    sys.exit("Maximum limit of trials exceeded")

        if size_list > 1:
            inp_list = []
            try_count = 3
            size_count = 1

            while size_list > 0:
                input_number = input("Enter value {0}: ".format(size_count))
                try:
                    input_number = int(input_number)
                    inp_list.append(input_number)
                    try_count = 3
                    size_list -= 1
                    size_count += 1

                except:
                    if try_count > 0:
                        print("Please enter value in integer to sort it!!")
                        try_count -= 1
                        print("{0} trials left!!".format(try_count))

                    else:
                        raise ValueError("Maximum limit of trials exceeded")

        else:
            raise ValueError("The size of list should be more than 1!!")

        print("The options for algorithm are: \n",
              "1.)  Quick sort \n",
              "2.)  Bubble sort \n",
              "3.)  Merge Sort \n")

        algo_index = int(input("Please enter the Sr. number of algorithm of your choice: "))

        if algo_index > 5:
            raise ValueError("Please enter valid chocie!!")

        list_algo = ["quick_sort", "bubble_sort", "merge_sort"]
        Main(list_algo[algo_index - 1], inp_list)
    
    except ValueError as e:
        print(e)
    
    except UnboundLocalError as ub:
        print("Exiting the code!!")

if __name__ == "__main__":
    main()
