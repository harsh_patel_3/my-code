'''
Quick sort
Bubble sort
BFS (I/P Graph)
DFS (I/P Graph)
Merge Sort
'''

class class_quick_sort:
    def partition(self, inp_list, low, high): 
        low_count = (low - 1)
        pivot = inp_list[high]
        for num_count in range(low, high): 
            if inp_list[num_count] <= pivot: 
                low_count = low_count + 1
                inp_list[low_count], inp_list[num_count] = inp_list[num_count], inp_list[low_count] 
        inp_list[low_count+1], inp_list[high] = inp_list[high], inp_list[low_count+1] 
        return low_count + 1

    def sort_fun(self, inp_list, low, high): 
        if low < high: 
            pivot = class_quick_sort.partition(self, inp_list,low,high) 
            class_quick_sort.sort_fun(self, inp_list, low, pivot-1) 
            class_quick_sort.sort_fun(self, inp_list, pivot+1, high) 

    def quick_sort(self, inp_list):
        class_quick_sort.sort_fun(self, inp_list, 0, len(inp_list) - 1)
        return inp_list

class class_bubble_sort:
    def sort_fun(self, inp_list):
        len_inp_list = len(inp_list)
        for len_count in range(len_inp_list):
            for len_count_1 in range(0, len_inp_list - len_count - 1):
                if inp_list[len_count_1] > inp_list[len_count_1 + 1]:
                    inp_list[len_count_1], inp_list[len_count_1 + 1] = inp_list[len_count_1 + 1], inp_list[len_count_1]
    def bubble_sort(self, inp_list):
        class_bubble_sort.sort_fun(self, inp_list)
        return inp_list

class class_merge_sort:
    def sort_fun(self, inp_list):
        if len(inp_list) > 1:
            mid = len(inp_list) // 2
            left = inp_list[:mid]
            right = inp_list[mid:]
            class_merge_sort.sort_fun(self, left)
            class_merge_sort.sort_fun(self, right)
            pivot_left = 0
            pivot_right = 0
            pivot = 0
            while pivot_left < len(left) and pivot_right < len(right):
                if left[pivot_left] < right[pivot_right]:
                    inp_list[pivot] = left[pivot_left]
                    pivot_left += 1
                else:
                    inp_list[pivot] = right[pivot_right]
                    pivot_right += 1
                pivot += 1
            while pivot_left < len(left):
                inp_list[pivot] = left[pivot_left]
                pivot_left += 1
                pivot += 1
            while pivot_right < len(right):
                inp_list[pivot]=right[pivot_right]
                pivot_right += 1
                pivot += 1
    # inp_list = [54,26,93,17,77,31,44,55,20]
    # class_merge_sort.sort_fun(inp_list)
    # print(input_list)



    def merge_sort(self, inp_list):
        class_merge_sort.sort_fun(self, inp_list)
        return inp_list
