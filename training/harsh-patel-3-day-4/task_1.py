'''
Create a python class Author.
Create a python class for Programming languages which must inherite Author class.
Create a class which should print the details of given programming language as input.
'''

class Author:
    def __init__(self, lang_name):
        if lang_name == 'java':
            self.author = "James Gosling"
        elif lang_name == 'python':
            self.author = "Guido van Rossum"
        elif lang_name == 'golang':
            self.author = "Robert Griesemer, Rob Pike and Ken Thompson"

    def get_author(self):
        return self.author

class Programming_Language(Author):
    def __init__(self, lang_name):
        Author.__init__(self, lang_name)

        if lang_name == 'java':
            self.details = "This is Java"
        elif lang_name == 'python':
            self.details = "This is Python"
        elif lang_name == 'golang':
            self.details = "This is GoLang"

    def get_details(self):
        return self.details

class Display:
    def display(self, lang_name):
        pl = Programming_Language(lang_name)
        print(pl.get_author())
        print(pl.get_details())

def main():
    lang_list = ["java", "python", "golang"]
    print("Available languages are: \n",
          "1.)  Java \n",
          "2.)  Python \n",
          "3.)  GoLang \n")
    try:
        lang_name = input("Enter the name of language: ")
        if lang_name.lower() in lang_list:
            Display().display(lang_name.lower())
        elif not lang_name:
            raise ValueError("Input can not be empty!!")
        else:
            raise ValueError("Incorrect option!!")
    except ValueError as e:
        print(e)

if __name__ == "__main__":
    main()
