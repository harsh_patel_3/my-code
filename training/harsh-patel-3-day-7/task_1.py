'''
Implement a python program using requests, httplib2 and
SOAP(with and without proxy) module to integrate ""PhishTank"" service.
PhishTank is a free online service, which stores information about Phishing URLs. 
The Input to the program should be URL
(e.g - http://www.travelswitchfly.com/ - Its a Phishing URL)
PhishTank API - https://www.phishtank.com/api_info.php
The output should tell us whether the input url is Phishing URL or not.
'''

import requests, json

def phish_search(URL):
    APP_KEY = "2517cc6d9560e45d2f56b5b2812d4d8b1234354e35c8b3846dd25a7421bc76bb"
    SEARCH = "https://checkurl.phishtank.com/checkurl/index.php"
    data = {
        "url": URL,
        "app_key": APP_KEY,
        "format": "json"
    }

    request = requests.post(url=SEARCH, data=data)

    validity = request.json()['meta']['status']

    if validity.lower() != 'error':
        in_database = request.json()['results']['in_database']

        if in_database:
            valid_url = request.json()['results']['valid']
            if valid_url:
                print("{0} is a phishing website!!".format(URL))

            else:
                print("{0} is not a phishing website...".format(URL))

        else:
            print("{0} is not in the database!!".format(URL))

    else:
        print("Please enter a valid URL!!")

if __name__ == "__main__":
    URL = input("Enter the URL to search: ")
    phish_search(URL)

    while True:
        print("Do you want to continue?")
        print("Press y|Y to continue.")
        print("Press n|N to exit.")
        OPTION = input("Enter your option: ")

        if OPTION in ('y', 'Y'):
            URL = input("Enter the URL to search: ")
            phish_search(URL)

        elif OPTION in ('n', 'N'):
            break

        else:
            print("Please enter a valid option!!")
