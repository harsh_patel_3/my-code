'''
Created by Harsh Patel 3 on 10 Dec 2019
email: harsh.patel3@crestdatasys.com

Write python script to take no of arguments as input from the user.
Then read no of arguments from the standard input.
Print read arguments on output.
'''

def data_operations():
    '''
    This is the first method to be invoken.
    '''
    try:
        # taking inputs
        no_of_inputs = int(input("Enter the number of inputs: "))

        if no_of_inputs < 1:
            raise ValueError("The number of inputs should be more than \"0\"")

        # defining the list to store inputs
        inputs = []

        # iterating through the total number of inputs
        while no_of_inputs > 0:
            value = input("Enter value: ")

            if not value:
                raise ValueError("The input should not be empty!!")

            inputs.append(value)
            no_of_inputs -= 1

        # iterating list to display inputs
        for input_count in inputs:
            print(input_count)

    except ValueError as e:
        if not e:
            print("The number of inputs should not be empty!")

        else:
            print(e)

if __name__ == "__main__":
    data_operations()
