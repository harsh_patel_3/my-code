'''
Created by Harsh Patel 3 on 10 Dec 2019
email: harsh.patel3@crestdatasys.com

Write python script to take one integer argument and then print as follows:
    - If Value > 0 and Value < 10 — Small
    - If Value > 10 and Value < 100 — Medium
    - If Value < 1000 — Large
    - If Value > 1000 — Invalid
'''

def check_integer():
    '''
    Checks integers
    '''
    try:
        # Taking input data
        input_no = int(input("Enter the number: "))

        # checking conditions
        if 0 < input_no < 10:
            print("Small")
        elif 10 < input_no < 100:
            print("Medium")
        elif 100 < input_no < 1000:
            print("Large")
        elif input_no > 1000:
            print("Invalid")
        else:
            print("The input should not be a negative number!")

    except ValueError:
        print("The input number should be a positive number!")

if __name__ == "__main__":
    check_integer()
