'''
Created by Harsh Patel 3 on 10 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a program to take two numbers as input parameter
and then ask for the arithmetic parameter to be performed.
i.e

"Enter Two numbers"
10 45
"Operations to perform"
+

Sum is 55
'''
def arithmatic():
    '''
    Performs arithamatic operations
    '''
    try:
        # getting the data
        number_1 = int(input("Enter the first number: "))
        number_2 = int(input("Enter the second number: "))

        # getting operators
        input_operator = input("Enter the operator you want: ")

        if not input_operator:
            raise ValueError("The operator should not be an empty string!!")

        input_operator = input_operator.strip()

        # making chocie
        if input_operator == "+":
            print("{0} + {1} = {2}".format(number_1, number_2, number_1 + number_2))

        elif input_operator == "-":
            print("{0} - {1} = {2}".format(number_1, number_2, number_1 - number_2))

        elif input_operator == "*":
            print("{0} * {1} = {2}".format(number_1, number_2, number_1 * number_2))

        elif input_operator == "/":
            print("{0} / {1} = {2}".format(number_1, number_2, round((number_1 / number_2), 2)))

        else:
            print("Invalid opeartor!!")

    except ValueError as e:
        if not e:
            print("The input number should be a number!")

        else:
            print(e)

    except ZeroDivisionError:
        print("The divisor should not be \"0\"")

if __name__ == "__main__":
    arithmatic()
