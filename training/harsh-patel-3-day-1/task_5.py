'''
Created by Harsh Patel 3 on 10 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a program to take two integers  as input.
Print those two integers as output and
then call a function to swap those two integers.
'''

def swap(number_one, number_two):
    '''
    This method swaps the numbers.
    '''
    return number_two, number_one

def main():
    '''
    This is the first method to be invoken.
    '''
    try:
        # taking inputs
        number_1 = int(input("Enter the first number: "))
        number_2 = int(input("Enter the second number: "))

        # before swapping
        print("Before swapping: \n"
            "     Number 1 is {0} \n"
            "     Number 2 is {1}".format(number_1, number_2))

        # calling the swap function
        new_number_1, new_number_2 = swap(number_1, number_2)

        # after swapping
        print("After swapping: \n"
            "     Number 1 is {0} \n"
            "     Number 2 is {1}".format(new_number_1, new_number_2))

    except ValueError:
        print("The input should be a number!!")

if __name__ == "__main__":
    main()
