'''
Created by Harsh Patel 3 on 10 Dec 2019
email: harsh.patel3@crestdatasys.com

Paresh owns a company that moves containers between two islands.
He has
    - N trips booked
    - each trip has P containers
    - Paresh has M boats for transporting containers
    - each boat's maximum capacity is C  containers

Given the number of containers going on each trip, determine whether or not Paresh
can perform all trips using no more than boats per individual trip.

If this is possible, print Yes; otherwise, print No.

Input Format:
    The first line contains three space-separated integers describing the respective values of
        - N(number of trips)
        - C (boat capacity)
        - M(total number of boats)
    The second line contains  space-separated integers describing
    the value for container for each trip.

Constraints
* 1<= m,c ,p<=100

Output Format

Print Yes if Paresh can perform
booked trips using no more than boats per trip; otherwise, print No.

Sample Input 0
    - 5 2 2
    - 1 2 1 4 3

Sample Output
    Yes

Sample Input
    - 5 1 2
    - 1 2 1 4 3

Sample Output
    No
'''

# importing module(s)
import sys

def calculate(number_of_trips, list_number_of_containers, boat_capacity, number_of_boats):
    '''
    Calculate the results based ob the injested data.
    '''
    # check condition : 1
    if number_of_trips == len(list_number_of_containers):
        trip_capacity = boat_capacity * number_of_boats
        # iterating through number of trips
        for trip_counter in range(number_of_trips):
            # check condition : 2
            # checking total number of containers
            if int(list_number_of_containers[trip_counter]) > 100:
                sys.exit("Total number of containers should not be more than 100!!")

            # check condition : 3
            if int(list_number_of_containers[trip_counter]) > trip_capacity:
                sys.exit("The boat is overloaded!!")

        print("The trip will be awesome!!")

    else:
        print("The total number of containers entered is more than the total number of trips!!")

def injest_data():
    '''
    This is the first method to be invoken.
    '''
    try:
        # entering the first set of data and splitting the string to list
        number_of_trips = int(input("Enter the numeber of trips: "))
        boat_capacity = int(input("Enter the boat capacity: "))
        number_of_boats = int(input("Enter the total number of boats: "))

        # checking boat capacity
        if boat_capacity < 1:
            sys.exit("The boat capacity can never be less than 1!!")

        # checking number of boats
        if number_of_boats < 1:
            sys.exit("Total number of boats can never be less than 1!!")

        print("\n")

        # entering the second set of data
        number_of_containers = input("Enter the number of containers in each trip: ")

        if not number_of_containers:
            raise ValueError("The number of containers should not be empty!!")

        list_number_of_containers = number_of_containers.split(" ")

        # checking the total number of args
        if len(list_number_of_containers) > number_of_trips:
            sys.exit("The entered number of containers are more than the total number of trips!!")
        elif len(list_number_of_containers) < number_of_trips:
            sys.exit("The entered number of containers are lesser than the total number of trips!!")

        calculate(number_of_trips, list_number_of_containers, boat_capacity, number_of_boats)

    except ValueError as e:
        if not e:
            print("The input should be a positive integer!!")

        else:
            print(e)

if __name__ == "__main__":
    injest_data()
