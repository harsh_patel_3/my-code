'''
Created by Harsh Patel 3 on 10 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a function to find larger of two numbers.
'''

def comapare_two_numbers():
    '''
    This is the first method to be invoken.
    '''
    try:
        # taking inputs
        number_1 = int(input("Enter number 1: "))
        number_2 = int(input("Enter number 2: "))

        if number_1 > number_2:
            print("{0} is greater than {1}".format(number_1, number_2))
        elif number_1 < number_2:
            print("{0} is greater than {1}".format(number_2, number_1))
        else:
            print("Both numbers are same!!")

    except ValueError:
        print("The input number should be a number!")

if __name__ == "__main__":
    comapare_two_numbers()
