from flask import Flask, render_template, request
app = Flask(__name__)

class Auth:
    cred = {'harsh': 'patel'}

    @staticmethod
    def auth(credentials):
        if credentials['name'] in Auth.cred.keys() and credentials['password'] in Auth.cred.values():
            return True
        else:
            return False

@app.route('/')
def get_cred(flag=1):
   return render_template('auth.html', flag=flag)

@app.route('/home',methods = ['POST', 'GET'])
def home(default = 0):
    try:
        if request.method == 'POST':
                credentials = request.form
                if Auth.auth(credentials):
                    return render_template("home.html",credentials = credentials)
                else:
                    return get_cred(flag=0)
    except:
        print("In except")
        return get_cred()

if __name__ == '__main__':
    app.run(debug = True)
