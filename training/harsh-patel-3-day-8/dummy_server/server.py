from flask import Flask
import random
import string
app = Flask(__name__)


@app.route('/urls/<id>')
def display_data(id):
    DATA_LIST = []
    for i in range(10):
        DATA_LIST.append(''.join(random.choice(string.ascii_lowercase)
                                 for length in range(10)))
    data = {
        id: DATA_LIST
    }
    return data


if __name__ == '__main__':
    app.run(debug=True, port=8080)
