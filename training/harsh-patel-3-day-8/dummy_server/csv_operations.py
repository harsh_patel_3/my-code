import csv, random, string, requests, json
from flask import render_template

LIST_URLS = []
def write_file():
    with open('ransomware.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        for count in range(1000):
            data = "http://127.0.0.1:8080/urls/{0}".format(count+1)
            writer.writerow([data])
    file.close()

def read_file():
    json_obj = {}
    csv_file = open('ransomware.csv', 'r')
    json_file = open('ransomware_data.json', 'w')
    reader = csv.reader(csv_file)
    for row in reader:
        _data = requests.get(row[0]).json()
        for key, val in _data.items():
            json_obj[key] = val
    json.dump(json_obj, json_file, indent=4)
    json_file.close()
    csv_file.close()

if __name__ == "__main__":
    write_file()
    read_file()
