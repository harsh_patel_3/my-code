'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a python function which counts the frequency of given
character in a given string.
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        input_string = input("Enter the input string: ")
        char_to_search = input("Enter the character to search: ")

        if not input_string:
            raise ValueError("The input string should not be an empty string!!")

        if not char_to_search:
            raise ValueError("The character to search should not be an empty string!!")

        # converting the string to lower case
        # searching any character by converting to lower case
        lower_search_counter = input_string.lower().count(char_to_search.lower())

        print("{0} is present {1} number of times"
            .format(char_to_search, lower_search_counter))

    except ValueError as e:
        print(e)

if __name__ == "__main__":
    main()
