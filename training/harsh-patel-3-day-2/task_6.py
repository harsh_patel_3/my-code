'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a program to take size of the list as input.
Then read as many values as input.
Store these details into list and print out list.
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        list_size = int(input("Enter the size of the list: "))

        input_list = []
        for size_counter in range(list_size):
            input_value = input("Enter value " + str(size_counter + 1) + ": ")

            if not input_value:
                raise ValueError("The input string should not be empty!!")

            input_list.append(input_value)

        print(input_list)

    except ValueError as e:
        if not e:
            print("The input string should be an integer!!")

        else:
            print(e)

if __name__ == "__main__":
    main()
