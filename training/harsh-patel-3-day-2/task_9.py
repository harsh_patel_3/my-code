'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Read a sentence from the standard input. Find out how many times each word appear in given string.
1.  i.e Input : “This is a Python learning”

2. Ouptput :
    This 1
    Is 1
    a 1
    Python 1
    Learning 1
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        input_string = input("Enter the string: ")

        if not input_string:
            raise ValueError("The input string should not be empty!!")

        string_list = input_string.split()
        dict_students = {}
        for word in string_list:
            if word in dict_students:
                dict_students[word] += 1

            else:
                dict_students[word] = 1

        for i in dict_students:
            print("{0} is repeated {1}".format(i, dict_students.get(i)))

    except ValueError as e:
        print(e)

if __name__ == "__main__":
    main()
