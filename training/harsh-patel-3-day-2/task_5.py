'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a Python function to insert a string in the middle of a string.
For odd length of string, remove the middle character and replace with given string.
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        first_input_string = input("Enter the first input string: ")

        if not first_input_string:
            raise ValueError("The first input string should not be empty!!")

        second_input_string = input("Enter the second input string: ")

        if not second_input_string:
            raise ValueError("The second input string should not be empty!!")

        middle_pos = int(len(first_input_string)//2)

        if len(first_input_string) % 2 == 0:
            new_word = "{0}{1}{2}".format(
                                first_input_string[:middle_pos],
                                second_input_string,
                                first_input_string[middle_pos:])

        else:
            new_word = "{0}{1}{2}".format(
                                first_input_string[:middle_pos],
                                second_input_string,
                                first_input_string[middle_pos + 1:])

        print(new_word)

    except ValueError as e:
        print(e)

if __name__ == "__main__":
    main()
