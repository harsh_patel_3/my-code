'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a Python function to remove the characters which have odd index values of a given string.
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        input_string = input("Enter the input string: ")

        if not input_string:
            raise ValueError("The input string should not be empty!!")

        new_string = ""

        for letter_count in range(len(input_string)):
            if letter_count % 2 == 0:
                new_string += input_string[letter_count]

        print(new_string)

    except ValueError as e:
        print(e)

if __name__ == "__main__":
    main()
