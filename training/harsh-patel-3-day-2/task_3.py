'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a Python program to add 'ing' at the end of a given string (length should be at least 3).
If the given string already ends with 'ing' then add 'ly' instead.
If the string length of the given string is less than 3, leave it unchanged.
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        input_string = input("Enter the input string: ")

        if not input_string:
            raise ValueError("Input can not be empty!!")

        elif len(input_string) < 3:
            print("The length of string is less than 3.")
            print("The original string is {0}"
                .format(input_string))

        elif input_string[-3 :] == "ing":
            input_string = input_string + "ly"
            print(input_string)

        else:
            input_string = input_string + "ing"
            print(input_string)

    except ValueError as e:
        print(e)

if __name__ == "__main__":
    main()
