'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a program to read names and grades of each student in class of N student.
Store these details into nested list.
Write a function to find student with second highest marks.
'''

def compare(list_stud):
    large = list_stud[0][1]
    s_large = 0

    for student in list_stud[1:]:
        if large < student[1] and s_large <= large:
            s_large = large
            large = student[1]
        elif large > student[1] and s_large < student[1]:
            s_large = student[1]
    print("Second largest number is {0}".format(s_large))

def main():
    '''
    main function to be executed.
    '''
    try:
        no_students = int(input("Enter total number of students: "))

        if no_students > 1:
            list_stud = []
            for count_stud in range(no_students):
                name = input("Enter the name of student "+
                            "{0}: ".format(count_stud + 1))

                if not name:
                    raise ValueError("The value of name should not be an empty string!!")

                grade = int(input("Enter the grade of student "+
                                "{0}: ".format(count_stud + 1)))

                if grade < 0:
                    raise ValueError("The grade should be a positive integer!!")

                list_stud.append([name, grade])
            compare(list_stud)
            
        else:
            print("Minimum number of students should be more than 1 to compare!!")

    except ValueError as e:
        if not e:
            print("The value should be an integer value!!")

        else:
            print(e)

if __name__ == "__main__":
    main()
