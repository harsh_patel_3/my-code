'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a Python program to get a string made of the first 2 and
the last 2 chars from a given a string.
If the string length is less than 2, return ""Empty String""
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        input_string = input("Enter the input string: ")

        if not input_string:
            raise ValueError("The input should not be empty!")

        if len(input_string) < 2:
            print("Empty String")
        else:
            new_str = input_string[:2] + input_string[-2:]
            print(new_str)

    except ValueError as e:
        print(e)

if __name__ == "__main__":
    main()
