'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a program a function for ATM machine which takes amount as
input and output should be number of notes of each denomination.
The ATM  has notes in following denomination : 2000, 500, 100.
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        amount = int(input("Enter the amount: "))

        notes2000 = notes500 = notes100 = 0
        if amount > 0:
            if amount >= 2000:
                notes2000 = int(amount/2000)
                amount = amount % 2000

            if amount >= 500:
                notes500 = int(amount/500)
                amount = amount % 500

            if amount >= 100:
                notes100 = int(amount/100)
                amount = amount % 100

            print(" {0} number of 2000 notes will be used.\n".format(notes2000),
                "{0} number of 500 notes will be used.\n".format(notes500),
                "{0} number of 100 notes will be used.\n".format(notes100),
                "{0} Rs. is left yet.".format(amount))

        else:
            print("The amount can not be negative!!")

    except ValueError:
        print("The amount should be a positive integer!!")

if __name__ == "__main__":
    main()
