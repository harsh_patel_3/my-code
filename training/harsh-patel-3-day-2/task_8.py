'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a program to find average of the list.
List should not contain non arithmetic values.
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        list_size = int(input("Enter the size of the list: "))
        input_list = []
        size_counter = 0
        sum_list = 0
        if list_size > 0:
            while size_counter < list_size:
                input_value = input("Enter value {0}: ").format(str(size_counter + 1))

                if not input_value:
                    raise ValueError("The input string should not be empty!!")

                input_list.append(input_value)

                if input_list[-1].isdigit():
                    sum_list += float(input_list[-1])
                    size_counter += 1

                else:
                    print("Non-arithmetic value!! Please re-enter value.")
                    input_list.pop()

            avg_list = float(sum_list / len(input_list))
            print("Average of the given inputs is: {0}".format(avg_list))

        else:
            print("The size of list cannot be \"0\"")

    except ValueError as e:
        if not e:
            print("The size of list can not be empty!")

        else:
            print(e)

if __name__ == "__main__":
    main()
