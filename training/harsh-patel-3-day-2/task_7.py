'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Write a program to find index of given input parameter in the list.
'''

def main():
    '''
    main function to be executed.
    '''
    try:
        list_size = int(input("Enter the size of the list: "))
        input_list = []

        if list_size > 0:
            for size_counter in range(list_size):
                input_value = input("Enter value " + str(size_counter + 1) + ": ")

                if not input_value:
                    raise ValueError("The input string should not be empty!!")

                input_list.append(input_value)

            char_to_find = input("Enter the value to search: ")

            if char_to_find in input_list:
                position = input_list.index(char_to_find)
                print("The position of {0} is {1}".format(char_to_find, position + 1))

            else:
                print("{0} is not in the ingested list!!".format(char_to_find))

        else:
            print("The size of list cannot be \"0\"")

    except ValueError as e:
        if not e:
            print("The size of list can not be empty!")

        else:
            print(e)

if __name__ == "__main__":
    main()
