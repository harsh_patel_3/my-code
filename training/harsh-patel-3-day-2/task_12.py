'''
Created by Harsh Patel 3 on 11 Dec 2019
email: harsh.patel3@crestdatasys.com

Create a program to take student information as input.
Student will have First Name, Last Name, Roll No.
Write a function to sort the list based on given input parameter.
i.e By First Name or Last Name or Roll No.
'''

def sort_details(stud_deatils):
    '''
    method to sort the details.
    '''
    print("By which format you want to sort the data?",
          "Press '1' to sort by first name",
          "Press '2' to sort by last name"
          "Press '3' to sort by roll number")
    choice = int(input("Enter your choice: "))

    stud_deatils.sort(key = lambda x: x[choice-1])
    print(stud_deatils)

def main():
    '''
    main method to execute.
    '''
    no_students = int(input("Enter total number of students: "))
    stud_deatils = []

    # for ingesting details of students
    for count_stud in range(no_students):
        fname = input("Enter the first name of student "+
                     "{0}: ".format(count_stud + 1))
        lname = input("Enter the last name of student "+
                      "{0}: ".format(count_stud + 1))
        roll_number = int(input("Enter the roll number of student "+
                                "{0}: ".format(count_stud + 1)))
        stud_deatils.append([fname, lname, roll_number])

    # calling function for the first time.
    sort_details(stud_deatils)

    # loop for option
    while True:
        print("Do you want to continue?")
        choice = input("Press y|Y to continue and n|N to exit: ")

        if choice in ('Y', 'y'):
            sort_details(stud_deatils)

        elif choice in ('N', 'n'):
            break

        else:
            print("Invalid chocie!!")
            break

if __name__ == "__main__":
    main()
