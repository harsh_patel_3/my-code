import sys

def ipv4(input_string):
    list_input = input_string.split(".")
    if len(list_input) == 4:
        for counter in list_input:
            if int(counter) > 255:
                sys.exit("Invalid IP address!!")
        print("The IP address {0} is correct!!".format(input_string))
    else:
        print("The IP address should be of length 4!!")

def ipv6(input_string):
    list_input = input_string.split(":")
    if len(list_input) == 4:
        pass
    else:
        print("The IP address should be of length 4!!")

def main():
    input_string = input("Enter the string: ")

    if len(input_string) > 0:
        if "." in input_string:
            ipv4(input_string)

        elif ":" in input_string:
            ipv6(input_string)

    else:
        print("The string can not be empty!!")

if __name__ == "__main__":
    main()