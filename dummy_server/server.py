from flask import Flask, request, render_template
app = Flask(__name__)

@app.route('/')
def display():
    data = request.args.get('data', default = "Empty", type = str)
    name = request.args.get('name', default = "Harsh", type = str)
    result = "{0} {1}".format(data, name)
    return render_template('home.html', data=result)

if __name__=='__main__':
    app.run(debug=True, port=8080)


'''
from flask import Flask, request
app = Flask(__name__)

@app.route('/')
def display():
    data = request.args.get('data', default = "Empty", type = str)
    name = request.args.get('name', default = "Harsh", type = str)
    result = data + name
    return result

if __name__=='__main__':
    app.run(debug=True, port=8080)
'''